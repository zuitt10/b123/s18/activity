let trainer = {
	name:"Jeric Moreno",
	age: 25,
	address: {
		city:"Bacolod City",
		country: "Philippines"
	},
	friends: [],
	pokemons: [],
	catch: function(user){		
			if(this.pokemons.length >= 6){
				console.log("A trainer should only have 6 pokemons to carry")
			} else {
				this.pokemons.push(user)
				console.log(`Gotcha,${user}`)
			}		
	},
	release: function(){
		if(this.pokemons.length > 0){
		this.pokemons.pop();
		console.log("You have remove a pokemon")
		} else{
			console.log("You have no more pokemon! Catch one first.")
		}
	}
}

function pokemon(name,type,level){
	this.name = name;
	this.type = type;
	this.level = level;
	this.hp = this.level * 3;
	this.atk = this.level * 2.5;
	this.def = this.level * 2;
	this.isFainted = false;

	this.tackle = function(obj){
		console.log(`${this.name} tackled ${obj.name}`)
	};
	this.faint = function(){
		console.log(`${this.name} has fainted.`)
		this.isFainted=  true;
	};
}

let  charizard = new pokemon("Charizard","Fire",10);
let  pikachu = new pokemon("Pikachu","Lightning",5);
console.log(charizard)
console.log(pikachu)

pikachu.tackle(charizard);